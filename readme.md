﻿# uConverter



![](help/screenshots/s1.png)





## О приложении

uConverter (читается `микроконвертер`) — приложение-конвертер чисел в разные системы счисления. Возможности:

- конвертирование чисел в системах счисления по основанию от 2 до 16,
- представление двоичного числа разрядностью до 32 бит,
- выполнение арифметических и логических бинарных операций над двумя операндами,
- гибкая настройка функционала и внешнего вида.





## Установка

Приложение не нуждается в установке (portable), работает из любого каталога. Скачать можно [отсюда](https://gitlab.com/riva-lab/uConverter/-/releases).





## Как пользоваться

Интерфейс приложения интуитивно понятный.





## Ответственность

Приложение предоставляется для свободного использования, без каких-либо гарантий и технической поддержки. Вы используете приложение по своему усмотрению и несете свою собственную ответственность за результаты его работы.





## Авторство

Copyright 2015, 2017, 2022 Riva, [FreeBSD License, modified](license.md). История версий — [versions.md](versions.md).

Разработано в [Free Pascal RAD IDE Lazarus](http://www.lazarus-ide.org) v1.6.4 r54278, компилятор [Free Pascal Compiler](https://freepascal.org) v3.0.2 x86_64-win64-win32/win64.

Значок приложения создан в [Iconion 2.7 Free](https://iconion.com).


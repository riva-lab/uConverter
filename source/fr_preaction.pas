unit fr_preaction;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, Controls, ExtCtrls, Buttons, ActnList, u_operations;

type

  { TfrActionPre }

  TfrActionPre = class(TFrame)
    acEmpty:    TAction;
    acNot:      TAction;
    acSwap:     TAction;
    acReverse:  TAction;
    acSwapOps:  TAction;
    acClear:    TAction;
    acSet:      TAction;
    acTo2:      TAction;
    acTo1:      TAction;
    alOpUnary:  TActionList;
    imOpUnary:  TImageList;
    pActionPre: TPanel;
    sbOpPEmpty: TSpeedButton;
    sbOpPRev:   TSpeedButton;
    sbOpPNot:   TSpeedButton;
    sbTo1:      TSpeedButton;
    sbTo2:      TSpeedButton;
    sbOpPSwap:  TSpeedButton;
    sbTo3:      TSpeedButton;
    sbTo4:      TSpeedButton;
    sbTo5:      TSpeedButton;

    procedure ModifyActionClick(Sender: TObject);
    procedure SingleActionClick(Sender: TObject);

    procedure SetOperandType(AValue: Integer);

  PRIVATE
    FOpIndex:     Integer;
    FOnClick:     TNotifyEvent;
    FOnCopyClick: TNotifyEvent;
    FOperandType: Integer;
    FSelected:    TOperationUnary;

  PUBLIC
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    property OnCopyClick: TNotifyEvent read FOnCopyClick write FOnCopyClick;

    property Selected: TOperationUnary read FSelected;
    property OpIndex: Integer read FOpIndex;
    property OperandType: Integer read FOperandType write SetOperandType;
  end;

implementation

{$R *.lfm}

{ TfrActionPre }

procedure TfrActionPre.ModifyActionClick(Sender: TObject);
  begin
    if Sender <> nil then
      begin
      case TComponent(Sender).Name of
        'acEmpty': FSelected   := ouNone;
        'acNot': FSelected     := ouNot;
        'acSwap': FSelected    := ouSwap;
        'acReverse': FSelected := ouReverse;
        end;

      if FOnClick <> nil then FOnClick(Self);
      end;
  end;

procedure TfrActionPre.SingleActionClick(Sender: TObject);
  begin
    if Sender <> nil then
      begin
      case TComponent(Sender).Name of
        'acTo1': FOpIndex     := 1;
        'acTo2': FOpIndex     := 2;
        'acSwapOps': FOpIndex := 3;
        'acClear': FOpIndex   := 4;
        'acSet': FOpIndex     := 5;
        end;

      if FOnCopyClick <> nil then FOnCopyClick(Self);
      end;
  end;

procedure TfrActionPre.SetOperandType(AValue: Integer);
  begin
    if FOperandType = AValue then Exit;
    FOperandType := AValue;

    acTo1.Visible     := (AValue = 2) or (AValue = 3);
    acTo2.Visible     := (AValue = 1) or (AValue = 3);
    acSwapOps.Visible := (AValue <> 3);
    acClear.Visible   := (AValue <> 3);
    acSet.Visible     := (AValue <> 3);
  end;

end.


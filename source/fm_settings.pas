unit fm_settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, ActnList, Controls, IniPropStorage, LCLTranslator, Buttons,
  CheckLst, LazFileUtils, LazUTF8, u_utilities, u_strings, u_converter;

const
  LANGUAGE_DEFAULT = 'RU, Russian - Русский';
  LANGUAGES_DIR    = DirectorySeparator + 'lang';
  LANGUAGES_FILE   = LANGUAGES_DIR + DirectorySeparator + 'languages.ini';
  SETTINGS_FILE    = DirectorySeparator + 'settings.ini';

type

  { TfmSettings }

  TfmSettings = class(TForm)
    lbCopyInfo:         TLabel;
    lbShowKind:         TLabel;
    lbLanguage:         TLabel;
    lbBits:             TLabel;
    lbShowMenuKind:     TLabel;
    lbSize:             TLabel;
    pBitBtn1:           TPanel;
    pShowMenu:          TPanel;
    pLanguage:          TPanel;
    pBBParam:           TPanel;
    pSystems:           TPanel;
    pButtons:           TPanel;
    pControls:          TPanel;
    rbShowMenuAll:      TRadioButton;
    rbShowBitBtn:       TRadioButton;
    rbShowMenu:         TRadioButton;
    rbShowFields:       TRadioButton;
    rbShowAll:          TRadioButton;
    bbApply:            TBitBtn;
    bbCancel:           TBitBtn;
    bbDefaults:         TBitBtn;
    btnFieldFont:       TButton;
    btnBitBtnFont:      TButton;
    btnColorReset:      TButton;
    rbShowToolbar:      TRadioButton;
    sbColor0:           TSpeedButton;
    sbColor1:           TSpeedButton;
    Shape1:             TShape;
    Shape2:             TShape;
    Shape3:             TShape;
    cbLanguage:         TComboBox;
    cbBitBtnCount:      TComboBox;
    cbBitBtnSize:       TComboBox;
    cbMinimizeToTray:   TCheckBox;
    cbShowSplash:       TCheckBox;
    cbShowCopyButton:   TCheckBox;
    clSystems:          TCheckListBox;
    dlgColor:           TColorDialog;
    dlgFontBB:          TFontDialog;
    dlgFontField:       TFontDialog;
    IniStorageSettings: TIniPropStorage;
    edCopyPrefix:       TEdit;
    edCopySuffix:       TEdit;
    gbBBColor:          TGroupBox;
    gbCopyFmt:          TGroupBox;
    gbSysSelect:        TGroupBox;

    SettingsActionList: TActionList;
    acCancel:           TAction;
    acOK:               TAction;

    PageCtrl:     TPageControl;
    Shape4:       TShape;
    Shape5:       TShape;
    TabSheet1:    TTabSheet;
    tsBitButtons: TTabSheet;
    tsFields:     TTabSheet;
    tsGeneral:    TTabSheet;

    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure IniStorageSettingsRestore(Sender: TObject);
    procedure IniStorageSettingsSavingProperties(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure btnFontClick(Sender: TObject);
    procedure cbLanguageChange(Sender: TObject);
    procedure onSystemsClick(Sender: TObject);
    procedure onChangeColor(Sender: TObject);
    procedure TranslateComboBoxes;

  PRIVATE
    FBitBtnCount:    Integer;
    FBitBtnSize:     Integer;
    FLanguageIndex:  Integer;
    FColor0:         TColor;
    FColor1:         TColor;
    FFontBitBtn:     TFont;
    FFontFields:     TFont;
    FMinToTray:      Boolean;
    FShowBitBtn:     Boolean;
    FShowCopyButton: Boolean;
    FShowFields:     Boolean;
    FShowMenu:       Boolean;
    FShowTollbar:    Boolean;
    FSplash:         Boolean;
    FLanguage:       String;
    FPrefix:         array[0..TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS] of String;
    FSuffix:         array[0..TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS] of String;
    FSystems:        array[0..TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS] of Boolean;

    function GetPrefix(Index: Integer): String;
    function GetSuffix(Index: Integer): String;
    function GetSystems(Index: Integer): Boolean;

    procedure IniStorageLangLoad;
    procedure LoadComponentsFromFields;
    procedure LoadFieldsFromComponents;

  PUBLIC
    procedure LanguageChangeImmediately;

    property Language: String read FLanguage;

    property FontFields: TFont read FFontFields;
    property FontBitBtn: TFont read FFontBitBtn;

    property Splash: Boolean read FSplash;
    property MinToTray: Boolean read FMinToTray;
    property ShowMenu: Boolean read FShowMenu;
    property ShowTollbar: Boolean read FShowTollbar;

    property ShowBitBtn: Boolean read FShowBitBtn;
    property BitBtnCount: Integer read FBitBtnCount;
    property BitBtnSize: Integer read FBitBtnSize;
    property Color0: TColor read FColor0;
    property Color1: TColor read FColor1;

    property ShowFields: Boolean read FShowFields;
    property ShowCopyButton: Boolean read FShowCopyButton;
    property Systems[Index: Integer]: Boolean read GetSystems;
    property Prefix[Index: Integer]: String read GetPrefix;
    property Suffix[Index: Integer]: String read GetSuffix;
  end;

var
  fmSettings:     TfmSettings;
  IniStorageLang: TIniPropStorage;


implementation

{$R *.lfm}

{ TfmSettings }

procedure TfmSettings.FormCreate(Sender: TObject);
  var
    i: Integer;
  begin
    sbColor0.Caption := '0';
    sbColor1.Caption := '1';
    edCopyPrefix.Clear;
    edCopySuffix.Clear;

    for i := Low(FSystems) to High(FSystems) do
      begin
      clSystems.Items.Add((i + TNumberConverter.MIN_EDITS).ToString);
      FSystems[i] := False;
      FPrefix[i]  := '';
      FSuffix[i]  := '';
      end;

    clSystems.Checked[2 - TNumberConverter.MIN_EDITS]  := True;
    clSystems.Checked[10 - TNumberConverter.MIN_EDITS] := True;
    clSystems.Checked[16 - TNumberConverter.MIN_EDITS] := True;

    ComboBoxUpdateList(cbBitBtnCount, [
      '4', '8', '12', '16', '20', '24', '28', '32']);

    ComboBoxUpdateList(cbBitBtnSize, [
      '25', '30', '35', '40', '45', '50']);

    FFontBitBtn := TFont.Create;
    FFontFields := TFont.Create;

    IniStorageSettings.IniFileName := ExtractFileDir(ParamStrUTF8(0)) + SETTINGS_FILE;
    PageCtrl.ActivePageIndex       := 0;

    IniStorageLangLoad;
  end;

procedure TfmSettings.FormShow(Sender: TObject);
  var
    i, tmp: Integer;
  begin
    BeginFormUpdate;

    TranslateComboBoxes;

    if Sender <> nil then
      LoadComponentsFromFields;

    if Sender <> nil then
      Position := poDefault;

    EndFormUpdate;

    tmp := PageCtrl.ActivePageIndex;

    for i := 1 to PageCtrl.PageCount do
      begin
      BeginFormUpdate;
      PageCtrl.ActivePageIndex := i - 1;
      AutoSize                 := True;
      EndFormUpdate;

      Constraints.MinWidth  := Width;
      Constraints.MinHeight := Height;

      AutoSize := False;
      end;

    BeginFormUpdate;
    btnFontClick(nil);

    if Sender <> nil then
      Position := poMainFormCenter;

    PageCtrl.ActivePageIndex := tmp;

    EndFormUpdate;
  end;

procedure TfmSettings.FormClose(Sender: TObject; var CloseAction: TCloseAction);
  begin
    if ModalResult <> mrOk then acCancel.Execute;
  end;


procedure TfmSettings.LoadComponentsFromFields;
  var
    i: Integer;
  begin
    cbLanguage.ItemIndex     := FLanguageIndex;
    cbMinimizeToTray.Checked := FMinToTray;
    cbShowSplash.Checked     := FSplash;
    cbShowCopyButton.Checked := FShowCopyButton;
    rbShowBitBtn.Checked     := FShowBitBtn;
    rbShowFields.Checked     := FShowFields;
    rbShowAll.Checked        := (FShowBitBtn and FShowFields);
    rbShowMenu.Checked       := FShowMenu;
    rbShowToolbar.Checked    := FShowTollbar;
    rbShowMenuAll.Checked    := (FShowTollbar and FShowMenu);
    cbBitBtnCount.Text       := FBitBtnCount.ToString;
    cbBitBtnSize.Text        := FBitBtnSize.ToString;
    sbColor0.Color           := FColor0;
    sbColor1.Color           := FColor1;

    for i := Low(FSystems) to High(FSystems) do
      clSystems.Checked[i] := FSystems[i];

    if FFontFields <> nil then dlgFontField.Font.Assign(FFontFields);
    if FFontBitBtn <> nil then dlgFontBB.Font.Assign(FFontBitBtn);
  end;

procedure TfmSettings.LoadFieldsFromComponents;
  var
    i: Integer;
  begin
    FLanguageIndex  := cbLanguage.ItemIndex;
    FLanguage       := LowerCase(Copy(cbLanguage.Text, 1, 2));
    FMinToTray      := cbMinimizeToTray.Checked;
    FSplash         := cbShowSplash.Checked;
    FShowCopyButton := cbShowCopyButton.Checked;
    FShowBitBtn     := rbShowBitBtn.Checked or rbShowAll.Checked;
    FShowFields     := rbShowFields.Checked or rbShowAll.Checked;
    FShowMenu       := rbShowMenu.Checked or rbShowMenuAll.Checked;
    FShowTollbar    := rbShowToolbar.Checked or rbShowMenuAll.Checked;
    FBitBtnCount    := StrToInt(cbBitBtnCount.Text);
    FBitBtnSize     := StrToInt(cbBitBtnSize.Text);
    FColor0         := sbColor0.Color;
    FColor1         := sbColor1.Color;

    for i := Low(FSystems) to High(FSystems) do
      FSystems[i] := clSystems.Checked[i];

    FFontBitBtn.Assign(dlgFontBB.Font);
    FFontFields.Assign(dlgFontField.Font);
  end;


procedure TfmSettings.IniStorageSettingsRestore(Sender: TObject);
  var
    i: Integer;
  begin
    with IniStorageSettings do
      begin
      if not Active then Exit;

      // установки отображения систем счисления
      IniSection := 'Systems';

      if not ReadBoolean('SystemSaved', False) then
        begin
        cbBitBtnCount.Text := '8';
        cbBitBtnSize.Text  := '35';
        btnColorReset.Click;
        end
      else
        for i := Low(FSystems) to High(FSystems) do
          begin
          clSystems.Checked[i] := ReadBoolean('System' + i.ToString, False);
          FPrefix[i]           := ReadString('Prefix' + i.ToString, '');
          FSuffix[i]           := ReadString('Suffix' + i.ToString, '');
          end;

      IniSection := ''; // выход из текущей секции
      end;

    LoadFieldsFromComponents;
  end;

procedure TfmSettings.IniStorageSettingsSavingProperties(Sender: TObject);
  var
    i: Integer;
  begin
    with IniStorageSettings do
      begin
      if not Active then Exit;

      // установки отображения систем счисления
      IniSection := 'Systems';

      WriteBoolean('SystemSaved', True);
      for i := Low(FSystems) to High(FSystems) do
        begin
        WriteBoolean('System' + i.ToString, FSystems[i]);
        WriteString('Prefix' + i.ToString, FPrefix[i]);
        WriteString('Suffix' + i.ToString, FSuffix[i]);
        end;

      IniSection := ''; // выход из текущей секции
      end;

    LoadComponentsFromFields;
  end;

procedure TfmSettings.IniStorageLangLoad;
  var
    i, cnt: Integer;
  begin
    cbLanguage.Clear;
    cbLanguage.Items.Append(LANGUAGE_DEFAULT);

    IniStorageLang := TIniPropStorage.Create(nil);
    with IniStorageLang do
      begin
      IniFileName := ExtractFileDir(ParamStrUTF8(0)) + LANGUAGES_FILE;
      Active      := True;
      IniSection  := 'Languages List';

      // если приложение не нашло файл со списком локализаций - создаем его
      if not FileExistsUTF8(IniFileName) then
        begin
        WriteInteger('Count', 1);
        WriteString('L-1', LANGUAGE_DEFAULT);
        end;

      // считываем список локализаций, кроме 1-го пункта (язык по умолчанию)
      cnt := ReadInteger('Count', 1);
      cbLanguage.ItemIndex := 0;

      if cnt > 1 then
        for i := 2 to cnt do
          cbLanguage.Items.Append(ReadString('L-' + i.ToString, ''));
      end;
  end;


function TfmSettings.GetPrefix(Index: Integer): String;
  begin
    Result := '';
    if Index < Length(FPrefix) then
      Result := FPrefix[Index];
  end;

function TfmSettings.GetSuffix(Index: Integer): String;
  begin
    Result := '';
    if Index < Length(FSuffix) then
      Result := FSuffix[Index];
  end;

function TfmSettings.GetSystems(Index: Integer): Boolean;
  begin
    Result := False;
    if Index < Length(FSystems) then
      Result := FSystems[Index];
  end;


procedure TfmSettings.acOKExecute(Sender: TObject);
  begin
    LoadFieldsFromComponents;

    ModalResult := mrOk;
  end;

procedure TfmSettings.acCancelExecute(Sender: TObject);
  begin
    LoadComponentsFromFields;

    ModalResult := mrCancel;
  end;


procedure TfmSettings.btnFontClick(Sender: TObject);
  var
    dlg:   TFontDialog;
    item:  TSpeedButton;
    items: array [0..1] of TSpeedButton;

  begin
    if Sender <> nil then
      begin
      dlg := nil;
      case TComponent(Sender).Name of
        'btnBitBtnFont': dlg := dlgFontBB;
        'btnFieldFont': dlg  := dlgFontField;
        end;

      if dlg <> nil then
        with dlg do
          if Execute then
            case TComponent(Sender).Name of
              'btnBitBtnFont': dlgFontBB   := dlg;
              'btnFieldFont': dlgFontField := dlg;
              end;
      end;

    items[0] := sbColor0;
    items[1] := sbColor1;
    for item in items do
      with item.Constraints do
        begin
        item.Font.Assign(dlgFontBB.Font);
        MinHeight := String(cbBitBtnSize.Text).ToInteger;
        MinWidth  := MinHeight;
        MaxHeight := MinHeight;
        MaxWidth  := MinHeight;
        end;
  end;

procedure TfmSettings.onSystemsClick(Sender: TObject);
  const
    lock: Boolean = False;
  var
    i: Integer;
  begin
    if lock then Exit;
    lock := True;

    i := clSystems.ItemIndex;

    if Sender <> nil then
      if (i >= Low(FSystems)) and (i <= High(FSystems)) then
        begin
        case TComponent(Sender).Name of
          'clSystems':
            begin
            FSystems[i] := clSystems.Checked[i];

            edCopyPrefix.Caption := FPrefix[i];
            edCopySuffix.Caption := FSuffix[i];
            end;

          'edCopyPrefix':
            FPrefix[i] := edCopyPrefix.Caption;

          'edCopySuffix':
            FSuffix[i] := edCopySuffix.Caption;
          end;
        end;

    lock := False;
  end;

procedure TfmSettings.onChangeColor(Sender: TObject);

  procedure dialogColor(ASpeedButton: TSpeedButton);
    begin
      dlgColor.Color := ASpeedButton.Color;
      if dlgColor.Execute then
        ASpeedButton.Color := dlgColor.Color;
    end;

  begin
    if Sender <> nil then
      case TComponent(Sender).Name of

        'sbColor0': dialogColor(sbColor0);
        'sbColor1': dialogColor(sbColor1);

        'btnColorReset':
          begin
          sbColor0.Color := $8080FF;
          sbColor1.Color := $80FF80;
          end;
        end;
  end;

procedure TfmSettings.cbLanguageChange(Sender: TObject);
  begin
    LanguageChangeImmediately;

    // перерисовываем форму, чтобы более длинные метки полностью помещались
    FormShow(nil);
  end;

procedure TfmSettings.TranslateComboBoxes;
  begin
    //ComboBoxUpdateList(cb, []);
  end;


procedure TfmSettings.LanguageChangeImmediately;
  begin
    // применяем язык интерфейса не выходя из настроек
    SetDefaultLang(LowerCase(Copy(cbLanguage.Text, 1, 2)),
      ExtractFileDir(ParamStrUTF8(0)) + LANGUAGES_DIR);

    lbCopyInfo.Caption := MultiString(TXT_COPYINFO);
  end;


end.

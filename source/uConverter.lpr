program uTerminal;

{$mode objfpc}{$H+}

uses
 {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads,
 {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, fm_main, fm_settings, fm_about,
  app_ver, u_strings, fm_confirm, u_utilities, fr_about, fr_number,
  fr_preaction, fr_action, u_operations, u_converter
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TfmAbout, fmAbout);
  Application.CreateForm(TfmSettings, fmSettings);
  Application.CreateForm(TfmConfirm, fmConfirm);
  Application.Run;
end.


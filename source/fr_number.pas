unit fr_number;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, Buttons, StdCtrls,
  Graphics, u_utilities, u_converter, u_operations, strutils;

type

  TCopyEvent = procedure(Index: Integer; AValue: String) of object;

  { TfrNumber }

  TfrNumber = class(TFrame)
    GroupBox1: TGroupBox;
    imNumber:  TImageList;
    pBits:     TPanel;
    pCommon:   TPanel;
    pValues:   TPanel;

  PRIVATE
    FOnChange:       TNotifyEvent;
    FOnCopy:         TCopyEvent;
    FShowBits:       Boolean;
    FShowCopyButton: Boolean;
    FShowValues:     Boolean;
    LabelBit:        array of TLabel;
    ButtonBit:       array of TSpeedButton;
    Field:           array of TEdit;
    CopyButton:      array of TSpeedButton;
    LabelEdit:       array of TLabel;
    FSystems:        array of Boolean;
    FBits:           Integer;
    FConvert:        TNumberConverter;
    FColor0:         TColor;
    FColor1:         TColor;
    FColorRect:      Tcolor;
    FFontBitBtn:     TFont;
    FFontField:      TFont;
    FModifyLast:     TOperationUnary;
    FModify:         TOperationUnary;
    FSize:           Integer;
    FReadOnly:       Boolean;
    FUpdateLock:     Boolean;

    function GetBit(Index: Integer): Boolean;
    function GetSystems(Index: Integer): Boolean;
    procedure SetBit(Index: Integer; AValue: Boolean);
    procedure SetBits(AValue: Integer);
    procedure SetColor0(AValue: TColor);
    procedure SetColor1(AValue: TColor);
    procedure SetColorRect(AValue: Tcolor);
    procedure SetFontBitBtn(AValue: TFont);
    procedure SetFontField(AValue: TFont);
    procedure SetModify(AValue: TOperationUnary);
    procedure SetShowBits(AValue: Boolean);
    procedure SetShowCopyButton(AValue: Boolean);
    procedure SetShowValues(AValue: Boolean);
    procedure SetSize(AValue: Integer);
    procedure SetSystems(Index: Integer; AValue: Boolean);
    procedure CreateButton(Index: Integer);
    procedure CreateLabel(Index: Integer);
    procedure CreateField(Index: Integer; AView: Boolean);
    procedure CreateCopyButton(Index: Integer; AView: Boolean);
    procedure CreateLabelEdit(Index: Integer; AView: Boolean);
    procedure OnBitClick(Sender: TObject);
    procedure OnValueChange(Sender: TObject);
    procedure OnCopyClick(Sender: TObject);
    procedure UpdateFrame;
    procedure UpdateBits;

  PUBLIC
    procedure Init(AReadOnly: Boolean = False);
    procedure Update;
    procedure SetValue(AValue: UInt32);

    property Bits: Integer read FBits write SetBits;
    property Size: Integer read FSize write SetSize;
    property ShowBits: Boolean read FShowBits write SetShowBits;
    property ShowValues: Boolean read FShowValues write SetShowValues;
    property Color0: TColor read FColor0 write SetColor0;
    property Color1: TColor read FColor1 write SetColor1;
    property ColorRect: TColor read FColorRect write SetColorRect;
    property FontField: TFont read FFontField write SetFontField;
    property FontBitBtn: TFont read FFontBitBtn write SetFontBitBtn;
    property ShowCopyButton: Boolean read FShowCopyButton write SetShowCopyButton;

    property Convert: TNumberConverter read FConvert;
    property Bit[Index: Integer]: Boolean read GetBit write SetBit;
    property Systems[Index: Integer]: Boolean read GetSystems write SetSystems;
    property Modify: TOperationUnary read FModify write SetModify;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnCopy: TCopyEvent read FOnCopy write FOnCopy;
  end;

implementation

{$R *.lfm}

{ TfrNumber }

procedure TfrNumber.SetBits(AValue: Integer);
  var
    i: Integer;
  begin
    if AValue = FBits then Exit;
    if AValue > TNumberConverter.MAX_BITS then AValue := TNumberConverter.MAX_BITS;
    FBits := AValue;

    for i := High(ButtonBit) downto Low(ButtonBit) do
      begin
      CreateLabel(i);
      CreateButton(i);
      end;

    UpdateFrame;
  end;


procedure TfrNumber.SetColor0(AValue: TColor);
  begin
    if FColor0 = AValue then Exit;
    FColor0 := AValue;

    UpdateFrame;
  end;

procedure TfrNumber.SetColor1(AValue: TColor);
  begin
    if FColor1 = AValue then Exit;
    FColor1 := AValue;

    UpdateFrame;
  end;

procedure TfrNumber.SetColorRect(AValue: Tcolor);
  begin
    FColorRect    := AValue;
    pBits.Color   := FColorRect;
    pCommon.Color := FColorRect;
    pValues.Color := FColorRect;
  end;

procedure TfrNumber.SetFontBitBtn(AValue: TFont);
  begin
    if FFontBitBtn = AValue then Exit;
    FFontBitBtn := AValue;

    UpdateBits;
  end;

procedure TfrNumber.SetFontField(AValue: TFont);
  begin
    if FFontField = AValue then Exit;
    FFontField := AValue;

    UpdateFrame;
  end;

procedure TfrNumber.SetModify(AValue: TOperationUnary);
  var
    i:   TOperationUnary;
    tmp: UInt32;
  begin
    if FModify = AValue then Exit;
    FModify := AValue;
    tmp     := FConvert.Decimal;

    for i in [FModifyLast, FModify] do
      begin
      case i of
        ouNot: tmp     := OperationUnary(ouNot, tmp);
        ouReverse: tmp := OperationUnary(ouReverse, tmp, FBits);
        ouSwap: tmp    := OperationUnary(ouSwap, tmp, FBits);
        end;

      tmp := tmp and UInt32((UInt64(1) shl FBits) - 1);
      end;

    FModifyLast := FModify;
    SetValue(tmp);
  end;

procedure TfrNumber.SetShowBits(AValue: Boolean);
  begin
    if FShowBits = AValue then Exit;
    FShowBits     := AValue;
    pBits.Visible := FShowBits;
  end;

procedure TfrNumber.SetShowCopyButton(AValue: Boolean);
  begin
    if FShowCopyButton = AValue then Exit;
    FShowCopyButton := AValue;

    UpdateFrame;
  end;

procedure TfrNumber.SetShowValues(AValue: Boolean);
  begin
    if FShowValues = AValue then Exit;
    FShowValues     := AValue;
    pValues.Visible := FShowValues;
  end;

procedure TfrNumber.SetSize(AValue: Integer);
  begin
    if FSize = AValue then Exit;
    FSize := AValue;

    UpdateFrame;
  end;


function TfrNumber.GetBit(Index: Integer): Boolean;
  begin
    if ButtonBit[Index] <> nil then
      Result := ButtonBit[Index].Tag <> 0;
  end;

procedure TfrNumber.SetBit(Index: Integer; AValue: Boolean);
  begin
    if ButtonBit[Index] <> nil then
      ButtonBit[Index].Tag := CheckBoolean(AValue, 1, 0);

    UpdateFrame;
  end;

function TfrNumber.GetSystems(Index: Integer): Boolean;
  begin
    Result := False;
    if Index in [Low(FSystems)..High(FSystems)] then
      Result := FSystems[Index];
  end;

procedure TfrNumber.SetSystems(Index: Integer; AValue: Boolean);
  var
    i: Integer;
  begin
    Index := Index - TNumberConverter.MIN_EDITS;

    if Index in [Low(FSystems)..High(FSystems)] then
      FSystems[Index] := AValue;

    for i := Low(FSystems) to High(FSystems) do
      begin
      CreateLabelEdit(i, FSystems[i]);
      CreateField(i, FSystems[i]);
      CreateCopyButton(i, FSystems[i]);
      end;
  end;

procedure TfrNumber.SetValue(AValue: UInt32);
  var
    i: Integer;
  begin
    if FConvert.Decimal = AValue then Exit;
    FConvert.Decimal := AValue;

    with FConvert do
      for i := Low(FSystems) to High(FSystems) do
        if FSystems[i] and (Field[i] <> nil) then
          with Field[i] do
            begin
            OnChange := nil;
            { конвертирование из 10 в Х }
            Text     := Number[TNumberConverter.MIN_EDITS + i];
            OnChange := @OnValueChange;
            end;

    UpdateBits;
  end;


procedure TfrNumber.CreateButton(Index: Integer);
  begin
    FreeAndNil(ButtonBit[Index]);
    if Index >= FBits then Exit;

    ButtonBit[Index] := TSpeedButton.Create(Owner);
    with ButtonBit[Index] do
      begin
      Parent      := pBits;
      Name        := Self.Name + 'BB' + Index.ToString;
      Transparent := False;
      Flat        := True;
      Tag         := 0;
      Caption     := Tag.ToString;
      OnClick     := @OnBitClick;
      end;
  end;

procedure TfrNumber.CreateLabel(Index: Integer);
  begin
    FreeAndNil(LabelBit[Index]);
    if Index >= FBits then Exit;

    LabelBit[Index] := TLabel.Create(Owner);
    with LabelBit[Index] do
      begin
      Parent    := pBits;
      Caption   := Index.ToString;
      Name      := Self.Name + 'LB' + Index.ToString;
      Alignment := taCenter;
      Layout    := tlCenter;
      end;
  end;

procedure TfrNumber.CreateField(Index: Integer; AView: Boolean);

  function GetFieldWidth: Integer;
    var
      x:   Integer;
      tmp: TImage;
    begin
      if FBits = 0 then Exit(10);
      x   := trunc(ln((UInt64(1) shl FBits) - 1) / ln(Index + TNumberConverter.MIN_EDITS)) + 1;
      tmp := TImage.Create(self);
      tmp.Canvas.Font.Assign(FFontField);
      Result := 16 + tmp.Canvas.TextWidth(AddChar('0', '', x));
      FreeAndNil(tmp);
    end;

  begin
    FreeAndNil(Field[Index]);
    if not AView then Exit;

    Field[Index] := TEdit.Create(Owner);
    with Field[Index] do
      begin
      Font.Assign(FFontField);
      Parent    := pValues;
      Tag       := Index;
      Name      := Self.Name + 'FL' + Index.ToString;
      Alignment := taRightJustify;
      Text      := FConvert.Number[Index + TNumberConverter.MIN_EDITS];
      OnChange  := @OnValueChange;
      ReadOnly  := FReadOnly;

      Constraints.MinWidth := GetFieldWidth;
      Constraints.MaxWidth := Constraints.MinWidth;
      end;
  end;

procedure TfrNumber.CreateCopyButton(Index: Integer; AView: Boolean);
  begin
    FreeAndNil(CopyButton[Index]);
    if not AView then Exit;

    CopyButton[Index] := TSpeedButton.Create(Owner);
    with CopyButton[Index] do
      begin
      Parent      := pValues;
      Name        := Self.Name + 'CB' + Index.ToString;
      Transparent := False;
      Flat        := True;
      Caption     := '';
      Tag         := Index;
      Color       := Parent.Color;
      OnClick     := @OnCopyClick;
      imNumber.GetBitmap(0, Glyph);
      end;
  end;

procedure TfrNumber.CreateLabelEdit(Index: Integer; AView: Boolean);
  begin
    FreeAndNil(LabelEdit[Index]);
    if not AView then Exit;

    LabelEdit[Index] := TLabel.Create(Owner);
    with LabelEdit[Index] do
      begin
      Parent      := pValues;
      Name        := Self.Name + 'LE' + Index.ToString;
      Caption     := (Index + TNumberConverter.MIN_EDITS).ToString;
      Layout      := tlBottom;
      Alignment   := taRightJustify;
      Font.Italic := True;

      BorderSpacing.Around := 1;
      BorderSpacing.Left   := 8;
      end;
  end;


procedure TfrNumber.OnBitClick(Sender: TObject);
  begin
    if FReadOnly then Exit;

    with TSpeedButton(Sender) do
      Tag := CheckBoolean(Tag = 0, 1, 0);

    UpdateFrame;

    if FOnChange <> nil then FOnChange(Sender);
  end;

procedure TfrNumber.OnValueChange(Sender: TObject);
  var
    edit: TEdit;
    i:    Integer;
  begin
    if FReadOnly then Exit;

    if Sender <> nil then
      begin
      edit := TEdit(Sender);


      with FConvert do
        begin
        { конвертирование из X в 10 }
        Number[TNumberConverter.MIN_EDITS + TComponent(Sender).Tag] := String(edit.Text);

        { проверка ввода }
        if IsError then Exit;

        { конвертирование из 10 в X }
        for i := Low(FSystems) to High(FSystems) do
          if FSystems[i] and (i <> TComponent(Sender).Tag) then
            with Field[i] do
              begin
              OnChange := nil;
              Text     := Number[TNumberConverter.MIN_EDITS + i];
              OnChange := @OnValueChange;
              end;
        end;
      end;

    UpdateBits;

    if FOnChange <> nil then FOnChange(Sender);
  end;

procedure TfrNumber.OnCopyClick(Sender: TObject);
  var
    i: Integer;
  begin
    if FOnCopy <> nil then
      begin
      i := TSpeedButton(Sender).Tag;
      FOnCopy(i, Field[i].Text);
      end;
  end;

procedure TfrNumber.UpdateFrame;
  var
    i:   Integer;
    tmp: LongWord;
  begin
    if FUpdateLock then Exit;
    FUpdateLock := True;

    tmp := 0;
    for i := FBits - 1 downto 0 do
      if ButtonBit[i] <> nil then
        with ButtonBit[i] do
          begin
          tmp *= 2;
          tmp += Tag;

          Constraints.MinWidth  := FSize;
          Constraints.MinHeight := FSize;
          Constraints.MaxWidth  := FSize;
          Constraints.MaxHeight := FSize;

          Font.Assign(FFontBitBtn);
          Color   := CheckBoolean(Tag <> 0, FColor1, FColor0);
          Caption := Tag.ToString;
          end;

    for i := Low(FSystems) to High(FSystems) do
      if FSystems[i] then
        begin
        if Field[i] <> nil then
          Field[i].Font.Assign(FFontField);

        with CopyButton[i] do
          begin
          Constraints.MinWidth := Height;
          Constraints.MaxWidth := Height;
          Visible              := FShowCopyButton;
          end;
        end;

    SetValue(tmp);
    FUpdateLock := False;
  end;

procedure TfrNumber.UpdateBits;
  var
    i:   Integer;
    tmp: LongWord;
  begin
    tmp := FConvert.Decimal;

    for i := 0 to FBits - 1 do
      begin
      if ButtonBit[i] <> nil then
        with ButtonBit[i] do
          Tag := CheckBoolean((tmp and 1) > 0, 1, 0);

      tmp := tmp div 2;
      end;

    UpdateFrame;
  end;

procedure TfrNumber.Init(AReadOnly: Boolean);
  var
    i: Integer;
  begin
    FConvert   := TNumberConverter.Create(1);
    ShowValues := True;
    ShowBits   := True;

    SetLength(LabelBit, TNumberConverter.MAX_BITS);
    SetLength(ButtonBit, TNumberConverter.MAX_BITS);

    SetLength(Field, TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS + 1);
    SetLength(CopyButton, TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS + 1);
    SetLength(LabelEdit, TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS + 1);

    SetLength(FSystems, TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS + 1);
    for i := Low(FSystems) to High(FSystems) do
      FSystems[i] := False;

    FFontField  := TFont.Create;
    FFontBitBtn := TFont.Create;

    FFontField.Assign(Font);
    FFontBitBtn.Assign(Font);

    FOnChange   := nil;
    FOnCopy     := nil;
    FReadOnly   := AReadOnly;
    FColor0     := $8888FF;
    FColor1     := $88FF88;
    ColorRect   := clSilver;
    Systems[10] := True;
    Size        := 30;
    Bits        := 8;
    FModifyLast := ouNone;
    FModify     := ouNone;
    FUpdateLock := False;

    SetValue(FConvert.Decimal);
  end;

procedure TfrNumber.Update;
  begin
    UpdateFrame;
    UpdateBits;
  end;

end.

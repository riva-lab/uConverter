unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  ActnList, ComCtrls, Controls, Dialogs, ExtCtrls,
  Forms, Graphics, IniPropStorage, LazFileUtils, LazUTF8, Menus,
  LCLIntf, Clipbrd, SysUtils, Classes,
  fm_about, fm_confirm, fm_settings, fr_number, fr_action, fr_preaction,
  u_strings, u_utilities, u_operations, u_converter;

const
  HELP_DIR        = 'help';
  HELP_DIR_ONLINE = '-/blob/master/help';
  HELP_FILE       = 'uConverter-help';

type

  { TfmMain }

  TfmMain = class(TForm)
    acExit:         TAction;
    acFileHeader:   TAction;
    acHelpHeader:   TAction;
    acInfo:         TAction;
    acShowOnTop:    TAction;
    acSettings:     TAction;
    acReset:        TAction;
    acHelp:         TAction;
    acHelpMD:       TAction;
    acHelpNet:      TAction;
    acResetOps:     TAction;
    acMode1:        TAction;
    acMode2:        TAction;
    acModeHeader:   TAction;
    acModeToolbar:  TAction;
    acWebsite:      TAction;
    acOpHeader:     TAction;
    alActionList:   TActionList;
    AppProperties:  TApplicationProperties;
    frOperation:    TfrAction;
    frActionPre1:   TfrActionPre;
    frActionPre2:   TfrActionPre;
    frActionPre3:   TfrActionPre;
    frRes:          TfrNumber;
    frOp2:          TfrNumber;
    frOp1:          TfrNumber;
    imImages16:     TImageList;
    IniStorageMain: TIniPropStorage;
    MenuItem1:      TMenuItem;
    MenuItem10:     TMenuItem;
    MenuItem11:     TMenuItem;
    MenuItem12:     TMenuItem;
    MenuItem13:     TMenuItem;
    MenuItem14:     TMenuItem;
    MenuItem15:     TMenuItem;
    MenuItem16:     TMenuItem;
    MenuItem17:     TMenuItem;
    MenuItem18:     TMenuItem;
    MenuItem19:     TMenuItem;
    MenuItem2:      TMenuItem;
    MenuItem20:     TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem24: TMenuItem;
    MenuItem25: TMenuItem;
    MenuItem3:      TMenuItem;
    MenuItem4:      TMenuItem;
    MenuItem5:      TMenuItem;
    MenuItem6:      TMenuItem;
    MenuItem7:      TMenuItem;
    MenuItem8:      TMenuItem;
    MenuItem9:      TMenuItem;
    mmMainMenu:     TMainMenu;
    pmMode:         TPopupMenu;
    pToolbar:       TPanel;
    pOp2:           TPanel;
    pRes:           TPanel;
    pCalculator:    TPanel;
    pOp1:           TPanel;
    pmTray:         TPopupMenu;
    tiTrayIcon:     TTrayIcon;
    tbMain:         TToolBar;
    ToolButton1:    TToolButton;
    ToolButton2:    TToolButton;
    ToolButton3:    TToolButton;
    ToolButton4:    TToolButton;
    ToolButton5:    TToolButton;
    ToolButton6:    TToolButton;
    tbtnMode:       TToolButton;

    procedure acResetExecute(Sender: TObject);
    procedure FormChangeBounds(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure tiTrayIconClick(Sender: TObject);

    procedure actionAppGeneral(Sender: TObject);
    procedure actionCommon(Sender: TObject);
    procedure actionViewGeneral(Sender: TObject);
    procedure SettingsApply(Sender: TObject = nil);

  PRIVATE
    FWSPrevious: TWindowState;

  PRIVATE
    procedure SettingsSaveToIni;
    procedure SettingsLoadFromIni;
    procedure OnPreOperationClick(Sender: TObject);
    procedure OnOperationClick(Sender: TObject);
    procedure OnCopyToOperandClick(Sender: TObject);
    procedure OnCopyClick(Index: Integer; AValue: String);
    procedure ResultUpdate(AValue: UInt32);

    procedure LanguageChange;
  PUBLIC
  end;

var
  fmMain:  TfmMain;
  mmEmpty: TMainMenu;

implementation

{$R *.lfm}

{ TfmMain }

{ ***  Обработка событий главной формы  *** }

// инициализация
procedure TfmMain.FormCreate(Sender: TObject);
  begin
    //ReadAppInfo;
    IniStorageMain.IniFileName := ExtractFileDir(ParamStrUTF8(0)) + SETTINGS_FILE;

    mmEmpty := TMainMenu.Create(fmMain);
    Menu    := mmEmpty;
  end;

// появление формы главного окна на экране
procedure TfmMain.FormShow(Sender: TObject);
  var
    i:     Integer;
    items: array [0..2] of TfrActionPre;
  begin

    // начальная инициализация
    frOp1.Init;
    frOp2.Init;
    frRes.Init(True);

    frOp1.OnCopy := @OnCopyClick;
    frOp2.OnCopy := @OnCopyClick;
    frRes.OnCopy := @OnCopyClick;

    frOp1.OnChange      := @OnOperationClick;
    frOp2.OnChange      := @OnOperationClick;
    frOperation.OnClick := @OnOperationClick;

    items[0] := frActionPre1;
    items[1] := frActionPre2;
    items[2] := frActionPre3;
    for i := Low(items) to High(items) do
      begin
      items[i].OperandType := i + 1;
      items[i].OnClick     := @OnPreOperationClick;
      items[i].OnCopyClick := @OnCopyToOperandClick;
      end;

    tiTrayIcon.Icon := Application.Icon;
    pToolbar.BorderSpacing.CellAlignVertical := ccaCenter;

    SettingsLoadFromIni;
    SettingsApply;

    // заставка (если включена)
    fmAbout.ShowSplash(fmSettings.Splash);

    // предустановка состояний элементов
    actionViewGeneral(acShowOnTop);
    actionAppGeneral(nil);

    Caption           := fmAbout.AppIntName + ' v' + fmAbout.AppVersion;
    Application.Title := Caption;
    tiTrayIcon.Hint   := Caption;
    OnShow            := nil;       // выкл. обработчик, нужен только при запуске
    Position          := poDefault; // чтобы не менялась позиция окна при разворачивании из трея
    FormChangeBounds(nil);
  end;

// изменение состояния главного окна (свернуто, нормально, развернуто)
procedure TfmMain.FormChangeBounds(Sender: TObject);
  begin
    if OnShow <> nil then Exit;

    if WindowState <> wsMinimized then
      FWSPrevious := WindowState;

    if fmSettings.MinToTray and (WindowState = wsMinimized) then
      tiTrayIconClick(Sender);
  end;

// действие при попытке закрыть приложение
procedure TfmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    SettingsSaveToIni;
  end;


{ ***  Работа с хранилищем настроек  *** }

// сохранение настроек в файл INI
procedure TfmMain.SettingsSaveToIni;
  begin
    with IniStorageMain do
      begin
      if not Active then Exit;

      // параметры состояния формы и компонентов
      IniSection := 'Last Parameters';
      WriteInteger('WindowMainTop', fmMain.RestoredTop);
      WriteInteger('WindowMainLeft', fmMain.RestoredLeft);
      WriteInteger('WindowMainWidth', fmMain.RestoredWidth);
      WriteInteger('WindowMainHeight', fmMain.RestoredHeight);
      WriteInteger('WindowMainState', Ord(fmMain.WindowState));
      WriteBoolean('OnTop', acShowOnTop.Checked);

      IniSection := ''; // выход из текущей секции
      end;
  end;

// загрузка настроек из файла INI
procedure TfmMain.SettingsLoadFromIni;
  begin
    with IniStorageMain do
      begin

      // параметры состояния формы и компонентов
      IniSection    := 'Last Parameters';
      fmMain.Width  := ReadInteger('WindowMainWidth', 650);
      fmMain.Height := ReadInteger('WindowMainHeight', 420);
      fmMain.Top    := ReadInteger('WindowMainTop', (Screen.Height - Height) div 2);
      fmMain.Left   := ReadInteger('WindowMainLeft', (Screen.Width - Width) div 2);

      fmMain.WindowState  := TWindowState(ReadInteger('WindowMainState', 0));
      acShowOnTop.Checked := ReadBoolean('OnTop', False);

      IniSection := ''; // выход из текущей секции
      end;

    // на случай, если некорректные параметры положения формы
    if abs(Top) > Screen.Height - Height then Top := (Screen.Height - Height) div 2;
    if abs(Left) > Screen.Width - Width then Left := (Screen.Width - Width) div 2;
  end;

// сброс настроек
procedure TfmMain.acResetExecute(Sender: TObject);
  begin
    if fmConfirm.Show(TXT_RESET, WARN_RESET, [mbYes, mbNo], Self) <> mrYes then Exit;

    // при сбросе настроек отключаем хранилища
    IniStorageMain.Active                := False;
    fmSettings.IniStorageSettings.Active := False;

    // восстанавливаем настройки - удаляем файл настроек
    if FileExistsUTF8(IniStorageMain.IniFileName) then
      DeleteFileUTF8(IniStorageMain.IniFileName);
  end;


procedure TfmMain.tiTrayIconClick(Sender: TObject);
  begin
    if Showing then
      fmMain.Hide
    else
      begin
      WindowState := FWSPrevious;
      fmMain.Show;
      end;
  end;


{ ***  Обработчики событий операндов  *** }

procedure TfmMain.OnPreOperationClick(Sender: TObject);
  var
    actionPre: TfrActionPre;
    number:    TfrNumber;
  begin
    actionPre := TfrActionPre(Sender);
    case actionPre.Name of
      'frActionPre1': number := frOp1;
      'frActionPre2': number := frOp2;
      'frActionPre3': number := frRes;
      end;

    number.Modify := actionPre.Selected;
    OnOperationClick(Sender);
  end;

procedure TfmMain.OnCopyToOperandClick(Sender: TObject);

  procedure SwapOperands;
    var
      tmp: UInt32;
    begin
      tmp := frOp1.Convert.Decimal;
      frOp1.SetValue(frOp2.Convert.Decimal);
      frOp2.SetValue(tmp);
    end;

  procedure OperandAction(A: TfrActionPre; O1, O2: TfrNumber);
    begin
      case A.OpIndex of
        3: SwapOperands;
        4: O1.SetValue(0);
        5: O1.SetValue(UInt32.MaxValue)
        else
          O2.SetValue(O1.Convert.Decimal);
        end;
    end;

  begin
    case TComponent(Sender).Name of
      'frActionPre1':
        OperandAction(frActionPre1, frOp1, frOp2);

      'frActionPre2':
        OperandAction(frActionPre2, frOp2, frOp1);

      'frActionPre3':
        if frActionPre3.OpIndex = 1 then
          frOp1.SetValue(frRes.Convert.Decimal) else
          frOp2.SetValue(frRes.Convert.Decimal);
      end;

    OnOperationClick(Sender);
  end;

procedure TfmMain.OnCopyClick(Index: Integer; AValue: String);
  begin
    with fmSettings do
      Clipboard.AsText := Prefix[Index] + AValue + Suffix[Index];
  end;

procedure TfmMain.OnOperationClick(Sender: TObject);
  begin
    ResultUpdate(OperationBinary(frOperation.Selected,
      frOp1.Convert.Decimal, frOp2.Convert.Decimal));
  end;

// обновление результата
procedure TfmMain.ResultUpdate(AValue: UInt32);
  begin
    frRes.Modify := ouNone;
    frRes.SetValue(AValue);
    frRes.Modify := frActionPre3.Selected;
  end;


{ ***  Управление видом  *** }
procedure TfmMain.actionAppGeneral(Sender: TObject);
  begin
    BeginFormUpdate;

    if Sender <> nil then
      case TAction(Sender).Name of

        // сброс всех операндов
        'acResetOps':
          begin
          frActionPre1.acClear.Execute;
          frActionPre2.acClear.Execute;
          end;

        // режим отображения Конвертер
        'acMode1':
          acMode1.Checked := True;

        // режим отображения Калькулятор-2
        'acMode2':
          acMode2.Checked := True;

        // выбор режима отображения кнопкой тулбара
        'acModeToolbar':
          tbtnMode.PopupMenu.PopUp;

        end;

    // изменение видимости элементов в зависимости от режима
    acResetOps.Visible  := not acMode1.Checked;
    frOperation.Visible := not acMode1.Checked;
    pOp2.Visible        := not acMode1.Checked;
    pRes.Visible        := not acMode1.Checked;

    EndFormUpdate;
  end;


{ ***  Управление видом  *** }
procedure TfmMain.actionViewGeneral(Sender: TObject);
  begin
    BeginFormUpdate;
    case TAction(Sender).Name of

      // опция главной формы 'поверх всех окон'
      'acShowOnTop':
        FormStyle := CheckBoolean(acShowOnTop.Checked, fsSystemStayOnTop, fsNormal);

      end;

    FormChangeBounds(Sender);
    EndFormUpdate;
  end;


{ ***  Команды общие  *** }
procedure TfmMain.actionCommon(Sender: TObject);
  begin
    case TAction(Sender).Name of

      // завершение работы приложения
      'acExit':
        Close;

      // открыть окно настроек приложения
      'acSettings':
        begin
        if fmSettings.ShowModal = mrOk then
          SettingsApply;
        LanguageChange;
        end;

      // вызов справки html
      'acHelp':
        OpenURL('..' + DirectorySeparator + HELP_DIR + DirectorySeparator + HELP_FILE + '.html');

      // вызов справки markdown
      'acHelpMD':
        OpenURL('..' + DirectorySeparator + HELP_DIR + DirectorySeparator + HELP_FILE + '.md');

      // вызов справки онлайн
      'acHelpNet':
        OpenURL(APP_SITE_ADDRESS + '/' + HELP_DIR_ONLINE + '/' + HELP_FILE + '.md');

      // ссылка на репозиторий
      'acWebsite':
        OpenURL(APP_SITE_ADDRESS);

      // окно информации о приложении
      'acInfo':
        begin
        fmAbout.FormStyle := FormStyle;
        fmAbout.Show;
        end;
      end;
  end;


{ ***  Сервисные методы  *** }

// действие: применение настроек
procedure TfmMain.SettingsApply(Sender: TObject);
  var
    i:     Integer;
    item:  TfrNumber;
    items: array [0..2] of TfrNumber;
  begin
    BeginFormUpdate;
    AutoSize := False;

    with fmSettings do
        try
        fmMain.Menu    := TMainMenu(CheckBooleanPtr(ShowMenu, mmMainMenu, mmEmpty));
        tbMain.Visible := ShowTollbar;

        items[0] := frOp1;
        items[1] := frOp2;
        items[2] := frRes;
        for item in items do
          begin
          item.Color0         := Color0;
          item.Color1         := Color1;
          item.ColorRect      := clSilver;
          item.Bits           := BitBtnCount;
          item.Size           := BitBtnSize;
          item.FontBitBtn     := FontBitBtn;
          item.FontField      := FontFields;
          item.ShowBits       := ShowBitBtn;
          item.ShowValues     := ShowFields;
          item.ShowCopyButton := ShowCopyButton;

          for i := 0 to TNumberConverter.MAX_EDITS - TNumberConverter.MIN_EDITS do
            item.Systems[TNumberConverter.MIN_EDITS + i] := Systems[i];
          end;

        except
        if fmConfirm.Show(TXT_ERROR, WARN_SETTINGS, mbYesNo, self) = mrYes then
          Close;
        end;

    AutoSize := True;
    LanguageChange;
    FormChangeBounds(Sender);
    EndFormUpdate;

    BeginFormUpdate;
    frOp1.Update;
    frOp2.Update;
    frRes.Update;
    EndFormUpdate;
  end;

// перевод интерфейса
procedure TfmMain.LanguageChange;
  begin
    fmSettings.LanguageChangeImmediately;
  end;


end.

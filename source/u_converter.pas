unit u_converter;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type

  { TNumberConverter }

  TNumberConverter = class
    const
    MAX_BITS  = 32;       // максимум разрядов двоичного числа
    MIN_EDITS = 2;        // минимальная СЧ
    MAX_EDITS = 16;       // максимальная СЧ

  PRIVATE
    FDecimal: UInt32;
    FIsError: Boolean;

    const
    CDigits = '0123456789abcdefghijklmnopqrstuvwxyz';

    var
    FNumber:    String;
    FNumberRaw: String;
    FUpperCase: Boolean;

    function CharToNumber(AChar: Char): Integer;
    function NumberToChar(ANumber: UInt32): Char;
    function Convert(AInput: String; AInSys, AOutSys: Integer): String;

    function GetNumber(SystemIndex: Integer): String;
    procedure SetNumber(SystemIndex: Integer; AValue: String);
    procedure SetDecimal(AValue: UInt32);

  PUBLIC
    constructor Create;
    constructor Create(ANumber: UInt32);
    destructor Destroy; OVERRIDE;

    procedure Inc;
    procedure Dec;

    property Number[SystemIndex: Integer]: String read GetNumber write SetNumber;
    property Decimal: UInt32 read FDecimal write SetDecimal;
    property UpperCase: Boolean read FUpperCase write FUpperCase;
    property IsError: Boolean read FIsError;
  end;

implementation


{ TNumberConverter }

function TNumberConverter.CharToNumber(AChar: Char): Integer;
  var
    i: Integer;
  begin
    AChar := LowerCase(AChar);

    for i := 1 to CDigits.Length do
      if (AChar = CDigits[i]) then
        Exit(i - 1);

    Result := -1;
  end;

function TNumberConverter.NumberToChar(ANumber: UInt32): Char;
  begin
    if ANumber <= CDigits.Length then
      Exit(CDigits[ANumber + 1]);

    Result := ' ';
  end;

function TNumberConverter.Convert(AInput: String; AInSys, AOutSys: Integer): String;
  var
    i, x:     Integer;
    _decimal: UInt32 = 0;
  begin
    FIsError := True;
    if AInSys < 2 then Exit('');
    if Length(AInput) = 0 then Exit('');

      try
      for i := 1 to Length(AInput) do
        begin
        _decimal *= AInSys;
        x        := CharToNumber(AInput[i]);
        if (x in [0..AInSys - 1]) then
          _decimal += x
        else
          Exit('');
        end;

      FDecimal := _decimal;
      Result   := '';
      if FDecimal = 0 then
        begin
        FIsError := False;
        Exit('0');
        end;

      while (_decimal > 0) do
        begin
        Result   := NumberToChar(_decimal mod AOutSys) + Result;
        _decimal := _decimal div AOutSys;
        end;
      except
      Result := '';
      end;
    FIsError := False;
  end;

function TNumberConverter.GetNumber(SystemIndex: Integer): String;
  begin
    Result := Convert(FNumber, 10, SystemIndex);
    if FUpperCase then Result := Result.ToUpper;
  end;

procedure TNumberConverter.SetNumber(SystemIndex: Integer; AValue: String);
  begin
    if AValue = FNumberRaw then Exit;

    if AValue = '' then
      begin
      FIsError := True;
      Exit;
      end;

    FIsError   := False;
    FNumberRaw := AValue;
    FNumber    := Convert(FNumberRaw, SystemIndex, 10);
  end;


procedure TNumberConverter.SetDecimal(AValue: UInt32);
  begin
    if StrToDWord(FNumber) = AValue then Exit;
    Number[10] := AValue.ToString;
  end;

constructor TNumberConverter.Create;
  begin
    FNumber    := '0';
    FNumberRaw := '0';
    FUpperCase := True;
  end;

constructor TNumberConverter.Create(ANumber: UInt32);
  begin
    Create;
    Decimal := ANumber;
  end;

destructor TNumberConverter.Destroy;
  begin
    inherited Destroy;
  end;

procedure TNumberConverter.Inc;
  begin
    FNumber := (StrToDWord(FNumber) + 1).ToString;
  end;

procedure TNumberConverter.Dec;
  begin
    FNumber := (StrToDWord(FNumber) - 1).ToString;
  end;

end.

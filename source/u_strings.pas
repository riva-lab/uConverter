unit u_strings;

{$mode objfpc}{$H+}
{
  Файл содержит многострочные текстовые метки.
  Если используется перевод с помощью LCLTranslator, то
  нельзя использовать в свойствах компонентов и ресурсов
  обычные многострочные метки, т.к. они некорректно будут
  отображены в файле перевода *.po (актуально для Lazarus v1.6.4).
  Чтобы это исправить, используется метки в виде одной строки,
  а необходимые символы переноса или иные служебные экранируются.
  Для использования таких ресурсов в приложении нужно
  обращаться к ним через функцию MultiString, которая
  преобразует эти строки в обычные.
}

interface

resourcestring

  WARN_SETTINGS = 'Не удалось применить все настройки!\n'
    + 'Возникла критическая ошибка. Перезапустите приложение.\n'
    + 'Завершить работу приложения?';

  WARN_RESET = 'При следующем запуске приложения будут загружены\n'
    + 'настройки по умолчанию. Продолжить?';

  //TXT_CONFIRM  = 'Подтверждение';
  //TXT_WARNING  = 'Предупреждение';
  TXT_ERROR = 'Ошибка';
  TXT_RESET = 'Сброс';

  TXT_COPYINFO = 'При копировании значения поля\n'
    + 'к нему будет добавлены\n'
    + 'указанные префикс и суффикс.';

function MultiString(input: String): String;

implementation

function MultiString(input: String): String;
  var
    i, f: Integer;
  begin
    Result := '';
    f      := 0;
    for i := 1 to Length(input) do
      begin
      if f = 1 then
        begin
        // https://en.wikipedia.org/wiki/Control_character
        // https://ru.wikipedia.org/wiki/Управляющие_символы
        case input[i] of
          '\': Result += '\';
          '0': Result += #0;
          'b': Result += #8;
          't': Result += #9;
          'n': Result += LineEnding;
          'r': Result += #13;
          'e': Result += #27;
          end;
        f := 0;
        Continue;
        end;

      if input[i] = '\' then
        f      := 1 else
        Result += input[i];
      end;
  end;

end.


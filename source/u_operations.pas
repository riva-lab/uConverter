unit u_operations;

{$mode ObjFPC}{$H+}

interface

uses
  SysUtils, StrUtils;

type

  TOperationUnary = (
    ouNone,
    ouNot, ouMinus, ouSwap, ouReverse);

  TOperationBinary = (
    obNone,
    obAdd, obSub, obMul, obDiv, obMod,
    obAnd, obNand, obOr, obNor, obXor, obXnor,
    obLsh, obRsh);


function OperationUnary(AOperation: TOperationUnary;
  AOperand: UInt32; ABits: Integer = 32): UInt32;

function OperationBinary(AOperation: TOperationBinary;
  AOperand1, AOperand2: UInt32): UInt32;


implementation


function Swap(AValue: String; AWidth: Integer): String;
  begin
    AValue := AddChar('0', AValue, AWidth);

    Result :=
      Copy(AValue, AWidth div 2 + 1, AWidth) +
      Copy(AValue, 1, AWidth div 2);
  end;

function Reverse(AValue: String; AWidth: Integer): String;
  var
    i: Integer;
  begin
    AValue := AddChar('0', AValue, AWidth);
    Result := AValue;

    for i := 1 to AWidth do
      Result[AWidth + 1 - i] := AValue[i];
  end;


function OperationUnary(AOperation: TOperationUnary; AOperand: UInt32;
  ABits: Integer): UInt32;
  begin
    case AOperation of
      ouNot: Result     := UInt32(not AOperand);
      ouMinus: Result   := UInt32(-Int32(AOperand));
      ouSwap: Result    := StrToDWord('%' + Swap(IntToBin(AOperand, ABits), ABits));
      ouReverse: Result := StrToDWord('%' + Reverse(IntToBin(AOperand, ABits), ABits));
      else
        Result := AOperand;
      end;
  end;

function OperationBinary(AOperation: TOperationBinary; AOperand1, AOperand2: UInt32): UInt32;
  begin
    if (AOperation in [obDiv, obMod]) and (AOperand2 = 0) then
      Result := 0
    else
      case AOperation of
        obAdd: Result  := UInt32(AOperand1 + AOperand2);
        obSub: Result  := UInt32(AOperand1 - AOperand2);
        obMul: Result  := UInt32(AOperand1 * AOperand2);
        obDiv: Result  := AOperand1 div AOperand2;
        obMod: Result  := AOperand1 mod AOperand2;
        obAnd: Result  := AOperand1 and AOperand2;
        obNand: Result := not (AOperand1 and AOperand2);
        obOr: Result   := AOperand1 or AOperand2;
        obNor: Result  := not (AOperand1 or AOperand2);
        obXor: Result  := AOperand1 xor AOperand2;
        obXnor: Result := not (AOperand1 xor AOperand2);
        obLsh: Result  := UInt32(AOperand1 shl AOperand2);
        obRsh: Result  := UInt32(AOperand1 shr AOperand2)
        else
          Result       := 0
        end;
  end;

end.

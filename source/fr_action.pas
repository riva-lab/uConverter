unit fr_action;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, Controls, ExtCtrls, Buttons, ActnList, u_operations;

type

  { TfrAction }

  TfrAction = class(TFrame)
    acAdd:     TAction;
    acSub:     TAction;
    acMul:     TAction;
    acDiv:     TAction;
    acMod:     TAction;
    acOr:      TAction;
    acNor:     TAction;
    acAnd:     TAction;
    acNand:    TAction;
    acLsh:     TAction;
    acRsh:     TAction;
    acEmpty:   TAction;
    acXnor:    TAction;
    acXor:     TAction;
    alOpBin:   TActionList;
    imOpBin:   TImageList;
    pAction:   TPanel;
    sbOpEmpty: TSpeedButton;
    sbOpNand:  TSpeedButton;
    sbOpXor:   TSpeedButton;
    sbOpXnor:  TSpeedButton;
    sbOpLsh:   TSpeedButton;
    sbOpRsh:   TSpeedButton;
    sbOpAdd:   TSpeedButton;
    sbOpSub:   TSpeedButton;
    sbOpMul:   TSpeedButton;
    sbOpDiv:   TSpeedButton;
    sbOpMod:   TSpeedButton;
    sbOpOr:    TSpeedButton;
    sbOpNor:   TSpeedButton;
    sbOpAnd:   TSpeedButton;

    procedure sbOpClick(Sender: TObject);

  PRIVATE
    FOnClick:  TNotifyEvent;
    FSelected: TOperationBinary;

  PUBLIC
    property OnClick: TNotifyEvent read FOnClick write FOnClick;

    property Selected: TOperationBinary read FSelected;
  end;

implementation

{$R *.lfm}

{ TfrAction }

procedure TfrAction.sbOpClick(Sender: TObject);
  begin
    if Sender <> nil then
      begin
      case TComponent(Sender).Name of
        'acEmpty': FSelected := obNone;
        'acAdd': FSelected   := obAdd;
        'acSub': FSelected   := obSub;
        'acMul': FSelected   := obMul;
        'acDiv': FSelected   := obDiv;
        'acMod': FSelected   := obMod;
        'acOr': FSelected    := obOr;
        'acNor': FSelected   := obNor;
        'acAnd': FSelected   := obAnd;
        'acNand': FSelected  := obNand;
        'acXor': FSelected   := obXor;
        'acXnor': FSelected  := obXnor;
        'acLsh': FSelected   := obLsh;
        'acRsh': FSelected   := obRsh;
        end;

      if FOnClick <> nil then FOnClick(Sender);
      end;
  end;

end.
